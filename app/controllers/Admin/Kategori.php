<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('logged_in') != TRUE){
			$notif = array(
				'status' => "gagal",
				'message' => "Silahkan login terlebih dahulu",
			);
			$this->session->set_flashdata($notif);
			redirect('panel');
		}
		$this->load->model('m_kategori');
	}

	public function index()
	{
		// $data = array('kategori' => $this->m_kategori->list_kategori('kategori')->result());
		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/admin/master/kategori/index');
		$this->load->view('dashboard/footer');
	}

	function data(){
		$result = $this->m_kategori->list_kategori('kategori');
		echo json_encode($result);
	}

	function tambah(){
		$data = array('nama_kategori' => $this->input->post('naker'));

		$result = $this->m_kategori->create($data,'kategori');
		echo json_encode($result);
	}

	function edit(){
		$id = $this->input->get('id');
		$result = $this->m_kategori->get($id,'kategori');
		echo json_encode($result);
	}

	function update(){
		$where = array('id_kategori' => $this->input->post('idker'));
		$data = array('nama_kategori' => $this->input->post('naker'));

		$result = $this->m_kategori->replace($where,$data,'kategori');
		echo json_encode($result);
	}

	function hapus(){
		$id = $this->input->post('kode');

		$result = $this->m_kategori->trash($id,'kategori');
		echo json_encode($result);
	}

}

/* End of file Kategori.php */
/* Location: ./application/controllers/Kategori.php */