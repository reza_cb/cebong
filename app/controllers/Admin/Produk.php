<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('logged_in') != TRUE){
			$notif = array(
				'status' => "gagal",
				'message' => "Silahkan login terlebih dahulu",
			);
			$this->session->set_flashdata($notif);
			redirect('login');
		}
		if($this->session->userdata('akses') != "Admin"){
			if($this->session->userdata('logged_in') != TRUE){
				$notif = array(
					'status' => "gagal",
					'message' => "Silahkan login terlebih dahulu",
				);
				$this->session->set_flashdata($notif);
				redirect('login');
			}else{
				$notif = array(
					'status' => "gagal",
					'message' => "Maaf akses anda tidak diijinkan untuk mengakses menu ini",
				);
				$this->session->set_flashdata($notif);
				redirect('Dashboard');
			}
		}
		$this->load->model('m_produk');
	}

	public function index()
	{
		$data['produk'] = $this->m_produk->list_produk()->result();

		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/admin/produk/index',$data);
		$this->load->view('dashboard/footer');
	}

	function tambah(){
		$img = $_FILES['gambar']['name'];
		$config['upload_path'] = './assets/images/produk/';
		$config['file_name'] = $this->input->post('produk');
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['overwrite'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$config['file_ext_tolower'] = TRUE;
		$this->load->library('upload',$config);

		// var_dump($img);

		if(!$this->upload->do_upload('gambar')){
			$notif = array(
				'status' => "gagal",
				'message' => $this->upload->display_errors(),
			);
			$this->session->set_flashdata($notif);
			redirect('Admin/Produk');
		}else{
			$data = array(
				'nama_produk' => $this->input->post('produk'),
				'deskripsi_produk' => $this->input->post('deskripsi'),
				'path_produk' => $this->upload->data('file_name'),
				'url_produk' => $this->input->post('url'),
			);

			$this->m_produk->create($data,'produk');
			redirect('Admin/Produk');
		}
	}

	function edit($id){
		$where = array('id_produk' => $id);
		$data['produk'] = $this->m_produk->get($where,'produk');

		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/admin/produk/edit',$data);
		$this->load->view('dashboard/footer');
	}

	function update(){
		$img = $_FILES['gambar']['name'];
		$config['upload_path'] = './assets/images/produk/';
		$config['file_name'] = $this->input->post('produk');
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['remove_spaces'] = TRUE;
		$config['file_ext_tolower'] = TRUE;
		$config['overwrite'] = TRUE;
		$this->load->library('upload',$config);

		if($img == NULL){
			$where = array('id_produk' => $this->input->post('id'));
			$data = array(
				'nama_produk' => $this->input->post('produk'),
				'url_produk' => $this->input->post('url'),
				'deskripsi_produk' => $this->input->post('deskripsi'),
			);

			$this->m_produk->replace($where,$data,'produk');
			redirect('Admin/Produk');
		}else{
			if(!$this->upload->do_upload('gambar')){
				$notif = array(
					'status' => "gagal",
					'message' => $this->upload->display_errors(),
				);
				$this->session->set_flashdata($notif);
				redirect('Admin/Produk');
			}else{
				$where = array('id_produk' => $this->input->post('id'));
				$data = array(
					'nama_produk' => $this->input->post('produk'),
					'url_produk' => $this->input->post('url'),
					'deskripsi_produk' => $this->input->post('deskripsi'),
					'path_produk' => $this->upload->data('file_name'),
					'slug_produk' => slug($this->input->post('produk')),
				);

				$this->m_produk->replace($where,$data,'produk');
				redirect('Admin/Produk');
			}
		}
	}

	function hapus($id){
		$where = array('id_produk' => $id);

		$this->m_produk->trash($where,'produk');
		redirect('Admin/Produk');
	}

}

/* End of file Produk.php */
/* Location: ./application/controllers/Produk.php */