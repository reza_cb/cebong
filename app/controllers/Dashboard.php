<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('logged_in') != TRUE){
			$notif = array(
				'status' => "gagal",
				'message' => "Maaf silahkan login terlebih dahulu !",
			);
			$this->session->set_flashdata($notif);
			redirect('panel');
		}
	}

	public function index()
	{
		$this->load->view('dashboard/sidebar');
		$this->load->view('dashboard/index');
		$this->load->view('dashboard/footer');
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */