<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_auth');
	}

	public function login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$cek = $this->m_auth->cek_user($username,'user');

		if($cek->num_rows() > 0){
			$row = $cek->row();
			if(password_verify($password,$row->password)){
				$session = array(
					'id' => $row->id_user,
					'username' => $row->username,
					'email' => $row->email,
					'path' => $row->path,
					'nama' => $row->nama_user,
					'jk' => $row->jk,
					'alamat' => $row->alamat,
					'akses' => $row->akses,
					'logged_in' => TRUE,
				);
				$this->session->set_userdata($session);
				redirect('Dashboard');
			}else{
				$notif = array(
					'status' => "gagal",
					'message' => "Maaf password yang anda masukkan salah",
				);
				$this->session->set_flashdata($notif);
				redirect('panel');
			}
		}else{
			$notif = array(
				'status' => "gagal",
				'message' => "Maaf ".$username." tidak terdafatr",
			);
			$this->session->set_flashdata($notif);
			redirect('panel');
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */