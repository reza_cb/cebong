<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Home extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			$this->load->model('m_produk');
		}
	
		public function index()
		{
			$this->load->library('pagination');
			$this->load->model('m_artikel');

			$total_produk = $this->m_produk->jumlah_produk();
			$config1['total_rows'] = $total_produk;
			$config1['per_page'] = 3;

			$total_artikel = $this->m_artikel->jumlah_artikel();
			$config2['total_rows'] = $total_artikel;
			$config2['per_page'] = 3;
			$this->pagination->initialize($config1,$config2);

			$data = array(
				'artikel' => $this->m_artikel->artikel_home($config2['per_page'])->result(),
				'produk' => $this->m_produk->produk_home($config1['per_page'])->result(),
			);
			$this->load->view('home/header');
			$this->load->view('home/index',$data);
			$this->load->view('home/footer');
		}

		public function about(){
			$this->load->view('home/header');
			$this->load->view('home/about');
			$this->load->view('home/footer');
		}

		public function contact(){
			$this->load->view('home/header');
			$this->load->view('home/contact');
			$this->load->view('home/footer');
		}

		public function produk(){
			$this->load->library('pagination');
			$total = $this->m_produk->jumlah_produk();
			$config['base_url'] = base_url('artikel/');
			$config['total_rows'] = $total;
			$config['per_page'] = 5;
			$from = $this->uri->segment(3);

			$config['first_link']       = 'First';
	        $config['last_link']        = 'Last';
	        $config['next_link']        = 'Next';
	        $config['prev_link']        = 'Prev';
	        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
	        $config['full_tag_close']   = '</ul></nav></div>';
	        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
	        $config['num_tag_close']    = '</span></li>';
	        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
	        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
	        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
	        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
	        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
	        $config['prev_tagl_close']  = '</span>Next</li>';
	        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
	        $config['first_tagl_close'] = '</span></li>';
	        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
	        $config['last_tagl_close']  = '</span></li>';
			$this->pagination->initialize($config);
			$data['produk'] = $this->m_produk->tampil_produk($config['per_page'],$from);

			$this->load->view('home/header');
			$this->load->view('home/produk/produk',$data);
			$this->load->view('home/footer');
		}

		function baca($id){
			$where = array('nama_produk' => $id);
			$data['produk'] = $this->m_produk->read($where,'produk');

			$this->load->view('home/header');
			$this->load->view('home/produk/read',$data);
			$this->load->view('home/footer');
		}

		public function artikel(){
			$this->load->view('home/header');
			$this->load->view('home/artikel/artikel');
			$this->load->view('home/footer');
		}

		public function order(){
			$this->load->view('home/header');
			$this->load->view('home/order');
			$this->load->view('home/footer');
		}

		public function login(){
			$this->load->view('login');
		}

		public function register(){
			$this->load->view('register');
		}
	
	}
	
	/* End of file Home.php */
	/* Location: ./application/controllers/Home.php */