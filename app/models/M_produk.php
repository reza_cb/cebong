<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_produk extends CI_Model {

	function produk_home($number){
		return $this->db->get('produk',$number);
	}

	function jumlah_produk(){
		return $this->db->get('produk')->num_rows();
	}

	function tampil_produk($number,$offset){
		return $this->db->get('produk',$number,$offset)->result();
	}

	function read($where,$table){
		$this->db->where($where);
		return $this->db->get($table)->row_array();
	}

	function list_produk(){
		return $this->db->get('produk');
	}

	function create($data,$table){
		$this->db->insert($table,$data);
	}

	function get($where,$table){
		$this->db->where($where);
		return $this->db->get($table)->row_array();
	}

	function replace($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	function trash($where,$table){
		$this->db->where($where);
		$row = $this->db->get($table)->row();
		unlink("./assets/images/produk/".$row->path_produk);
		$this->db->delete($table,$where);
	}

}

/* End of file M_produk.php */
/* Location: ./application/models/M_produk.php */