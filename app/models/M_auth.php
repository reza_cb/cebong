<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_auth extends CI_Model {

	function cek_user($username,$table){
		$this->db->select('user.*,detail_user.*');
		$this->db->join('detail_user','detail_user.id_user=user.id_user');
		$this->db->where('username',$username);
		return $this->db->get($table);
	}

}

/* End of file M_auth.php */
/* Location: ./application/models/M_auth.php */