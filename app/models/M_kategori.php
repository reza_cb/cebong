<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_kategori extends CI_Model {

	function list_kategori($table){
		$this->db->order_by('id_kategori','ASC');
		return $this->db->get($table)->result();
	}	

	function create($data,$table){
		$this->db->insert($table,$data);
	}

	function get($id,$table){
		$this->db->where('id_kategori',$id);
		return $this->db->get($table)->row_array();
	}

	function replace($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	function trash($id,$table){
		$this->db->where('id_kategori',$id);
		$this->db->delete($table);
	}

}

/* End of file M_kategori.php */
/* Location: ./application/models/M_kategori.php */