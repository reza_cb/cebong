<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_artikel extends CI_Model {

	function artikel_home($number){
		return $this->db->get('artikel',$number);
	}

	function jumlah_artikel(){
		return $this->db->get('artikel')->num_rows();
	}

}

/* End of file M_artikel.php */
/* Location: ./application/models/M_artikel.php */