<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">
						<h3>Edit Form Produk</h3>
					</div>
					<div class="box-body">
						<?php echo form_open_multipart('Admin/Produk/update') ?>
							<div class="form-group">
								<div class="col-md-6">
									<label class="control-label">Nama Produk :</label>
									<input type="text" name="produk" class="form-control" value="<?php echo $produk['nama_produk'] ?>" required>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6">
									<label class="control-label">URL Produk :</label>
									<input type="text" name="url" class="form-control" value="<?php echo $produk['url_produk'] ?>">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6">
									<label class="control-label">Deskripsi Produk :</label>
									<textarea name="deskripsi" id="summernote"><?php echo $produk['deskripsi_produk'] ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6">
									<label class="control-label">Gambar Produk :</label><br>
									<img src="<?php echo base_url('assets/images/produk/'.$produk['path_produk']) ?>" width="200px" heigth="150px">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label class="control-label">Gambar Pengganti :</label>
									<input type="file" name="gambar" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6">
									<input type="hidden" name="id" class="form-control" value="<?php $produk['id_produk'] ?>" readonly>
									<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>