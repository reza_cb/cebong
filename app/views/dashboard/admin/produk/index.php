<div class="content-wrapper">
	<section class="content-header">
		<h1>Daftar Produk</h1><small>Cebong Solution</small>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<?php if($this->session->flashdata('status') == "gagal"){ ?>
					<div class="alert alert-danger"><?php echo $this->session->flashdata('message') ?></div>
				<?php } ?>
				<div class="box">
					<div class="box-header">
						<a href="#" data-toggle="modal" data-target="#tambah" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
					</div>
					<div class="box-body">
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Produk</th>
									<th class="text-center">URL</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach($produk as $p){ ?>
								<tr>
									<td class="text-center"><?php echo $no++ ?></td>
									<td><?php echo $p->nama_produk ?></td>
									<td><?php echo $p->url_produk ?></td>
									<td class="text-center">
										<a href="<?php echo site_url('Admin/Produk/edit/'.$p->id_produk) ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
										<a href="#" data-url="<?php echo site_url('Admin/Produk/hapus/'.$p->id_produk) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal Tambah -->
		<div class="modal fade" tabindex="-1" role="dialog" id="tambah">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title">Tambah Produk</h5>
		      </div>
		      <?php echo form_open_multipart('Admin/Produk/tambah') ?>
		      <div class="modal-body">
		        <div class="form-group">
		        	<label class="control-label">Nama Produk :</label>
		        	<input type="text" name="produk" class="form-control" required>
		        </div>
		        <div class="form-group">
		        	<label class="control-label">URL</label>
		        	<input type="text" name="url" class="form-control" required>
		        </div>
		        <div class="form-group">
		        	<label class="control-label">Deskripsi Produk :</label>
		        	<textarea name="deskripsi" id="summernote" required></textarea>
		        </div>
		        <div class="form-group">
		        	<label class="control-label">Gambar Produk :</label>
		        	<input type="file" name="gambar" id="upload">
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
		      </div>
		      </form>
		    </div>
		  </div>
		</div>
		<!-- Modal Tambah -->
	</section>
</div>
<script type="text/javascript">
	// Hapus
    $(document).ready(function(){
      $('.confirm_delete').on('click', function(){
        
        var delete_url = $(this).attr('data-url');

        swal({
          title: "Hapus Produk",
          text: "Yakin ingin menghapus produk ini ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#FC0606",
          confirmButtonText: "Hapus !",
          cancelButtonText: "Batalkan",
          closeOnConfirm: false     
        }, function(){
          window.location.href = delete_url;
        });

        return false;
      });
    });
</script>