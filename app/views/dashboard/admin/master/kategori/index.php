<div class="content-wrapper">
	<section class="content-header">
		<h1>Daftar Kategori</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">
						<a href="#" data-toggle="modal" data-target="#tambah" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
					</div>
					<div class="box-body">
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th class="text-center">Kategori</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody id="show_data">
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal Tambah -->
		<div class="modal fade" tabindex="-1" role="dialog" id="tambah">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title">Tambah Data Kategori</h5>
		      </div>
		      <form>
		      <div class="modal-body">
		        <div class="form-group">
		        	<label class="control-label">Nama Kategori :</label>
		        	<input type="text" name="kategori" class="form-control" placeholder="Ex: Olahraga, News, Info" id="nama_kategori" required>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="submit" class="btn btn-primary" id="btn_simpan"><i class="fa fa-save"></i> Save</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
		      </div>
		      </form>
		    </div>
		  </div>
		</div>
		<!-- Modal Tambah -->
		<!-- Modal Edit -->
		<div class="modal fade" tabindex="-1" role="dialog" id="edit">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title">Edit Data Kategori</h5>
		      </div>
		      <form>
		      <div class="modal-body">
		        <div class="form-group">
		        	<label class="control-label">Nama Kategori :</label>
		        	<input type="hidden" name="id_edit" class="form-control" id="id_kategori" readonly>
		        	<input type="text" name="kategori_edit" class="form-control" placeholder="Ex: Olahraga, News, Info" id="kategori" required>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="submit" class="btn btn-primary" id="btn_update"><i class="fa fa-save"></i> Update</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
		      </div>
		      </form>
		    </div>
		  </div>
		</div>
		<!-- Modal Edit -->
		<!-- Modal Hapus -->
		<div class="modal fade" tabindex="-1" role="dialog" id="hapus">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title">Hapus Data Kategori</h5>
		      </div>
		      <form>
		      <div class="modal-body">
		        <div class="form-group">
		        	<input type="hidden" class="form-control" name="id" id="textkode" readonly>
                    <div class="alert alert-danger"><p>Apakah Anda yakin mau memhapus Kategori ini?</p></div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="submit" class="btn btn-danger" id="btn_hapus"><i class="fa fa-trash"></i> Delete</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
		      </div>
		      </form>
		    </div>
		  </div>
		</div>
		<!-- Modal Hapus -->
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		tampil_data_kategori();
		function tampil_data_kategori(){
			$.ajax({
                type  : 'ajax',
                url   : '<?php echo site_url('Admin/Kategori/data')?>',
                async : false,
                dataType : 'json',
                success : function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<tr>'+
                                '<td>'+data[i].nama_kategori+'</td>'+
                                '<td style="text-align:right;">'+
                                    '<a href="javascript:;" class="btn btn-info btn-xs item_edit" data="'+data[i].id_kategori+'"><i class="fa fa-edit"></i></a>'+' '+
                                    '<a href="javascript:;" class="btn btn-danger btn-xs item_hapus" data="'+data[i].id_kategori+'"><i class="fa fa-trash"></i></a>'+
                                '</td>'+
                                '</tr>';
                    }
                    $('#show_data').html(html);
                }
 
            });
		}

		//Simpan Barang
        $('#btn_simpan').on('click',function(){
            var naker=$('#nama_kategori').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('Admin/Kategori/tambah')?>",
                dataType : "JSON",
                data : {naker:naker},
                success: function(data){
                    $('[name="kategori"]').val("");
                    $('#tambah').modal('hide');
                    tampil_data_kategori();
                }
            });
            return false;
        });

        //Ambil Data Update
        $('#show_data').on('click','.item_edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo site_url('Admin/Kategori/edit')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    $.each(data,function(id_kategori, nama_kategori){
                        $('#edit').modal('show');
                        $('[name="id_edit"]').val(data.id_kategori);
                        $('[name="kategori_edit"]').val(data.nama_kategori);
                    });
                }
            });
            return false;
        });

         //Update Barang
        $('#btn_update').on('click',function(){
            var idker=$('#id_kategori').val();
            var naker=$('#kategori').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url('Admin/Kategori/update')?>",
                dataType : "JSON",
                data : {idker:idker , naker:naker},
                success: function(data){
                    $('[name="id_edit"]').val("");
                    $('[name="kategori_edit"]').val("");
                    $('#edit').modal('hide');
                    tampil_data_kategori();
                }
            });
            return false;
        });

		//Ambil data untuk hapus
        $('#show_data').on('click','.item_hapus',function(){
            var id=$(this).attr('data');
            $('#hapus').modal('show');
            $('[name="id"]').val(id);
        });

		//Hapus Barang
        $('#btn_hapus').on('click',function(){
            var kode=$('#textkode').val();
            $.ajax({
            type : "POST",
            url  : "<?php echo base_url('Admin/Kategori/hapus')?>",
            dataType : "JSON",
              data : {kode: kode},
              success: function(data){
                    $('#hapus').modal('hide');
                    tampil_data_kategori();
              }
           });
           return false;
        });
	})
</script>