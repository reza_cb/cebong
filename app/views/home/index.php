
  <div id="pageintro" class="hoc clear">
    <article>
      <h2 class="heading">Cebong Solution</h2>
      <p>Memberikan solusi untuk kebutuhan IT anda</p>
    </article>
  </div>
  <!-- ################################################################################################ -->
</div>
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <article class="one_quarter first">
      <h6 class="heading font-x3">Produk Kami</h6>
      <footer><a href="<?php echo site_url('produk') ?>" class="btn">Lihat Lebih Banyak &raquo;</a></footer>
    </article>
    <?php foreach($produk as $p){ ?>
    <article class="one_quarter">
      <h6 class="heading"><?php echo $p->nama_produk ?></h6>
      <a href="#"><img src="<?php echo base_url('assets/images/produk/'.$p->path_produk) ?>" style="width: 100px;height: 100px"  alt=""></a>
      <p class="btmspace-30"><?php 
        $text = $p->deskripsi_produk;
        $post = word_limiter($text,7);
        echo $post;
      ?></p>
      <footer><a href="<?php echo site_url('produk/read/'.$p->nama_produk) ?>">Read More &raquo;</a></footer>
    </article>
    <?php } ?>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row2">
  <div class="hoc container clear"> 
    <!-- ################################################################################################ -->
    <div class="group">
      <section class="one_half first"> 
        <!-- ################################################################################################ -->
        <h6 class="heading">Apa saja yang kami berikan ?</h6>
        <ul class="nospace group">
          <li class="one_half first">
            <article><a href="#"><i class="icon btmspace-30 fa fa-globe"></i></a>
              <h6 class="heading font-x1">Siap Diakses</h6>
              <p>Produk yang anda beli, akan langsung di hosting oleh kami dengan domain yang anda inginkan</p>
            </article>
          </li>
          <li class="one_half">
            <article><a href="#"><i class="icon btmspace-30 fa fa-comments"></i></a>
              <h6 class="heading font-x1">Support Tim</h6>
              <p>Tim kami siap membantu anda jika mengalami atau ingin melakukan maintenance produk yang anda beli</p>
            </article>
          </li>
        </ul>
        <!-- ################################################################################################ -->
      </section>
      <section class="one_half"> 
        <!-- ################################################################################################ -->
        <h6 class="heading">Produk Utama</h6>
        <p class="btmspace-30">Salah satu produk utama kami. Media pembelajaran yang memberikan pendekatan berbeda dari yang lain</p>
        <article class="group">
          <div class="one_half first"><a href="#"><img src="<?php echo base_url('assets/images/produk/ajarincode.png') ?>" alt=""></a></div>
          <div class="one_half">
            <h6 class="heading nospace font-x1">AjarinCode</h6>
            <p>AjarinCode merupakan media pembelajaran bahasa pemrograman berbasis web yang dapat diakses dimanapun dan kapanpun. Anda bisa mengatur waktu belajar dan dengan metode yang berbeda</p>
            <footer><a href="https://ajarincode.com" class="btn" target="_blank">Register Now</a></footer>
          </div>
        </article>
        <!-- ################################################################################################ -->
      </section>
    </div>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper bgded overlay" style="background-image:url('<?php echo base_url('assets/images/background2.jpg') ?>');">
  <article class="hoc container clear cta"> 
    <!-- ################################################################################################ -->
    <div class="three_quarter first">
      <h6 class="nospace">Ingin punya website ?</h6>
      <p class="nospace">Kami juga menyediakan jasa pembuatan website untuk membantu anda memanfaatkan teknologi</p>
    </div>
    <footer class="one_quarter"><a class="btn medium" href="<?php echo site_url('order') ?>">Cara Pesan &raquo;</a></footer>
    <!-- ################################################################################################ -->
  </article>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
  <section id="latest" class="hoc container clear"> 
    <!-- ################################################################################################ -->
    <div class="sectiontitle">
      <h2 class="heading">Artikel</h2>  
    </div>
    <ul class="nospace group">
      <?php foreach($artikel as $a){ ?>
      <li class="one_quarter">
        <article><img src="<?php echo base_url('assets/images/artikel/'.$a->gambar_artikel) ?>" alt="">
          <div class="postexcerpt">
            <h6 class="heading"><?php echo $a->judul_artikel ?></h6>
            <p><?php echo $a->isi_artikel ?></p>
          </div>
          <footer><a href="#">More</a></footer>
        </article>
      </li>
      <?php } ?>
    </ul>
    <div class="row">
      <p align="center"><a href="<?php echo site_url('artikel') ?>" class="btn">Load More</a></p>
    </div>
    <!-- ################################################################################################ -->
  </section>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->