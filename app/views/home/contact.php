</div>
<div class="wrapper row2">
  <div id="breadcrumb" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <ul>
      <li><a href="<?php echo site_url('index') ?>">Home</a></li>
      <li><a href="<?php echo site_url('contact') ?>">Contact</a></li>
    </ul>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="content"> 
      <!-- ################################################################################################ -->
      <h1>Butuh pertanyaan lebih lanjut ?</h1>
      
      <form action="#" method="post">
        <div class="col-md-6">
          <div class="form-group">
            <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" required>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <input type="email" name="email" class="form-control" placeholder="Email yang bisa dihubungi" required>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <textarea name="pesan" class="form-control" placeholder="Pesan atau pertanyaan" style="height: 250px" required></textarea>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> Kirim</button>
          </div>
        </div>
      </form>
      <!-- ################################################################################################ -->
    </div>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->