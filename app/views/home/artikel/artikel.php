</div>
<div class="wrapper row2">
  <div id="breadcrumb" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <ul>
      <li><a href="<?php echo site_url('index') ?>">Home</a></li>
      <li><a href="<?php echo site_url('produk') ?>">Produk</a></li>
    </ul>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="content"> 
      <!-- ################################################################################################ -->
      <div id="gallery">
        <figure>
          <header class="heading">Produk</header>
          <article class="one_quarter">
            <h6 class="heading">Pretium <a href="#">consectetur</a></h6>
            <img class="btmspace-15" src="<?php echo base_url() ?>assets/home/images/demo/320x240.png" alt="">
            <p class="btmspace-30">Cursus massa aenean tellus hendrerit laoreet dignissim velit aliquam tincidunt massa sapien curabitur gravida <a href="#">[&hellip;]</a></p>
            <footer><a href="#" class="btn btn-info">Read More &raquo;</a></footer>
          </article>
        </figure>
      </div>
      <!-- ################################################################################################ -->
      <!-- ################################################################################################ -->
      <nav class="pagination">
        <ul>
          <li><a href="#">&laquo; Previous</a></li>
          <li><a href="#">1</a></li>
          <li><a href="#">2</a></li>
          <li><strong>&hellip;</strong></li>
          <li><a href="#">6</a></li>
          <li class="current"><strong>7</strong></li>
          <li><a href="#">8</a></li>
          <li><a href="#">9</a></li>
          <li><strong>&hellip;</strong></li>
          <li><a href="#">14</a></li>
          <li><a href="#">15</a></li>
          <li><a href="#">Next &raquo;</a></li>
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </div>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>