</div>
<div class="wrapper row2">
  <div id="breadcrumb" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <ul>
      <li><a href="<?php echo site_url('index') ?>">Home</a></li>
      <li><a href="<?php echo site_url('order') ?>">Order</a></li>
    </ul>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="content"> 
      <!-- ################################################################################################ -->
      <div id="gallery">
        <figure>
          <header class="heading">Cara Order</header>
          <div class="col-md-4">
            1. Registrasi
            <p>Pertama lakukan registrasi terlebih dahulu melalui link berikut <br><a href="<?php echo site_url('register') ?>" class="btn btn-info">Registrasi</a></p>
          </div>
          <div class="col-md-4">
            2. Akses Menu Order
            <p>Lakukan Pemesanan melalui menu order dan isi semua form yang sudah disediakan untuk memudahkan kami dalam mengerjakan projek anda</p>
          </div>
          <div class="col-md-4">
            3. Pembayaran
            <p>Projek selesai silahkan lakukan pembayaran dan projek anda akan kami berikan</p>
          </div>
        </figure>
      </div>
      <!-- ################################################################################################ -->
    </div>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>