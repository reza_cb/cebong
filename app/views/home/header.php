<!DOCTYPE html>
<!--
Template Name: Ossibird
Author: <a href="http://www.os-templates.com/">OS Templates</a>
Author URI: http://www.os-templates.com/
Licence: Free to use under our free template licence terms
Licence URI: http://www.os-templates.com/template-terms
-->
<html>
<head>
<title>Cebong Official Website</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="<?php echo base_url() ?>assets/home/layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/home/bootstrap/css/bootstrap.min.css">
</head>
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Top Background Image Wrapper -->
<div class="topspacer bgded overlay" style="background-image:url('<?php echo base_url('assets/images/background.png') ?>');"> 
  <!-- ################################################################################################ -->
  <div class="wrapper row0">
    <div id="topbar" class="hoc clear"> 
      <!-- ################################################################################################ -->
      <div class="fl_left">
        <ul class="nospace">
          <li style="font-size: 15px"><i class="fa fa-whatsapp"></i> +(62)081805467977</li>
          <li style="font-size: 15px"><i class="fa fa-envelope-o"></i> info@cebong.site</li>
        </ul>
      </div>
      <div class="fl_right">
        <ul class="nospace">
          <li><a href="<?php echo site_url('index') ?>" style="font-size: 20px"><i class="fa fa-home"></i></a></li>
          <li><a href="<?php echo site_url('login') ?>" style="font-size: 20px">Login</a></li>
          <li><a href="<?php echo site_url('register') ?>" style="font-size: 20px">Register</a></li>
        </ul>
      </div>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- ################################################################################################ -->
  <!-- ################################################################################################ -->
  <!-- ################################################################################################ -->
  <div class="wrapper row1">
    <header id="header" class="hoc clear"> 
      <!-- ################################################################################################ -->
      <div id="logo" class="fl_left">
        <h1><a href="<?php echo site_url('index') ?>">Cebong Solution</a></h1>
      </div>
      <!-- ################################################################################################ -->
      <nav id="mainav" class="fl_right">
        <ul class="clear" id="nav">
          <li><a href="<?php echo site_url('index') ?>">Home</a></li>
          <li><a href="<?php echo site_url('about') ?>">About</a></li>
          <li><a href="<?php echo site_url('produk') ?>">Produk</a></li>
          <li><a href="<?pHp echo site_url('artikel') ?>">Artikel</a></li>
          <li><a href="<?php echo site_url('contact') ?>">Contact</a></li>
        </ul>
      </nav>
    </header>
    <!-- ################################################################################################ -->
  </div>
  <!-- ################################################################################################ -->
  <!-- ################################################################################################ -->
  <!-- ################################################################################################ -->
  
<!-- End Top Background Image Wrapper -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->