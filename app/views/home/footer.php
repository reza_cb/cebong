<div class="wrapper row4 bgded overlay" style="background-image:url('<?php echo base_url('assets/images/background.png') ?>');">
  <footer id="footer" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <div class="one_third first">
      <h6 class="heading">Cebong Solution</h6>
      
      <p>Cebong Solution menyediakan produk yang membantu anda dalam memenuhi kebutuhan IT anda.</p>
    </div>
    <div class="one_third">
      <h6 class="heading">Contact Tambahan</h6>
      <ul class="nospace linklist contact">
        <li><i class="fa fa-whatsapp"></i> (+62)081805467977</li>
        <li><i class="fa fa-envelope-o"></i> info@cebong.site</li>
      </ul>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p class="fl_left">Cebong Solution - All Rights Reserved - <a href="<?php echo site_url('index') ?>">Cebong Site</a></p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<script src="<?php echo base_url() ?>assets/home/layout/scripts/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/home/layout/scripts/jquery.backtotop.js"></script>
<script src="<?php echo base_url() ?>assets/home/layout/scripts/jquery.mobilemenu.js"></script>
<script src="<?php echo base_url() ?>assets/home/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
  $(function(){
    // Kontrol Menu
    $('#nav a[href~="' + location.href + '"]').parents('li').addClass('active');
  })
</script>
</body>
</html>