</div>
<div class="wrapper row2">
  <div id="breadcrumb" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <ul>
      <li><a href="<?php echo site_url('index') ?>">Home</a></li>
      <li><a href="<?php echo site_url('about') ?>">About</a></li>
    </ul>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="content"> 
      <!-- ################################################################################################ -->
      <h1>Tentang Cebong Solution</h1>
      <img class="imgr borderedbox inspace-5" src="<?php echo base_url() ?>assets/home/images/demo/imgr.gif" alt="">
      <p>Cebong Solution merupakan penyedia layanan yang membantu mempermudah pekerjaan sehari-hari. Kami selalu memprioritaskan kenyamanan pengguna kami dengan memberikan support dan solusi dalam permasalahan yang terjadi</p>
      <p>Kami juga menyediakan jasa pembuatan website untuk membantu meningkatkan pengalaman dan dapat memanfaatkan teknologi dengan baik</p>
      <p>Kami mencoba memberikan pelayanan dengan harga yang terjangkau tanpa mengurangi kualitas dan hormat kami terhadap pelanggan kami</p>
      <!-- ################################################################################################ -->
    </div>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->